# PaginaCurricular
## Esta es mi propia **página web**
---------
##### Hacerla _bonita_, _presentable_ y _fácil de entender_ era mi objetivo principal en mente, pues sirve a modo de presentación de mi imagen a todo aquel interesado en como soy yo y mi cuál es mi metodología.
---------
## Objetivos
* Mostrar mis habilidades de programación.
* Mostrar mis trabajos previos de diseño.
* Mostrar mis habilidades como FrontEnd developer.
  * Mediante demostración práctica en la propia página.
  * Mediante presentación de otros proyectos.
---------
>Esto solo es una carta de presentación, ergo, no aparece todo lo que aprendo día a día, sino tan solo una pequeña muestra de mi desarrollo.
---------
## Email de contacto
<jonathanrodenaslopez1@gmail.com> 
---------
## URL to the webpage
<loremipsum> 
 -
O este enlace si lo prefieres https://jonthan.xyz/

## My webpage appareance

![imagen](https://user-images.githubusercontent.com/57847247/133019347-2977d8d8-3c44-4098-8509-0d12e795446c.png)
 
 
![imagen](https://user-images.githubusercontent.com/57847247/133019381-91fba774-6778-4b47-bb80-c182bdf4796f.png)
 
 
![imagen](https://user-images.githubusercontent.com/57847247/133019469-6b89b18a-ba8e-416c-a732-453cd1e687ca.png)


